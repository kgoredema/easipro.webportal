package com.easipro.payrole;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EasiproApplication {

	public static void main(String[] args) {
		SpringApplication.run(EasiproApplication.class, args);
	}

}
