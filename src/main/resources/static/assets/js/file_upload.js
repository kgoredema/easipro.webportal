function validate_fileupload(fileName) {
	var allowed_extensions = new Array("csv");
	var file_extension = fileName.split('.').pop().toLowerCase(); 
	for (var i = 0; i <= allowed_extensions.length; i++) {
		if (allowed_extensions[i] == file_extension) {
			document.getElementById("saveButton").disabled = false;
			return true; // valid file extension
		}

	}
	document.getElementById("saveButton").disabled = true;
	alert("Wrong file  format!")
	return false;
}