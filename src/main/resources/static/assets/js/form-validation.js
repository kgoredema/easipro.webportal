$(document).ready(function () {
    $.formUtils.addValidator({
        name: 'required_if_visible',
        validatorFunction: function (value, $el, config, language, $form) {
            if ($el.is(":visible")) {
                return $.trim(value) !== '';
            } else {
                return true;
            }
        },
        errorMessage: 'Change reason is required',
        errorMessageKey: 'badVisibleValue'
    });


    $.validate({
        form: '#ilp-registration-form, #bank-edit-form, #branch-edit-form, #registration-search',
        validateOnBlur: false
    });
    
});



