$(document).ready(function () {

    const cronSchedulesListControl = $("#cron-expression");
    const cronExpressionControl = $("#cron-expression-result");
    const humanReadableCronControl = $("#cron-human-readable");

    initializeFormControls();

    function initializeFormControls() {

        const cronList = cronSchedulesListControl.val();

        cronList.split("#")
            .filter(cron => cron)
            .forEach(cron => {
            const cronHumanReadableExpression = `Runs - ${ prettyCron.toString(cron) }`;
            addSchedule(cron, cronHumanReadableExpression);
    })
    }


    $(".addScheduleBtn").click(function () {

        const rowCount = $('#cronSchedulesTableBody').find('tr').length;

        if (rowCount < 1) {

            const cronExpression = cronExpressionControl.val();

            if (expressionExists(cronExpression)) {

                alert("Expression already exists!");

            } else {
                const humanReadableExpression = humanReadableCronControl.text();
                addSchedule(cronExpression, humanReadableExpression, true);
            }
        } else {
            alert("Can only add at most 1 schedule");
        }
    });


    $(".delete-row").click(function () {

        const cronSchedulesTableBody = $("#cronSchedulesTableBody");

        cronSchedulesTableBody.find('input[name="record"]').each(function () {
            if ($(this).is(":checked")) {
                $(this).parents("tr").remove();
            }
        });

        const rows = cronSchedulesTableBody.find("tr");
        let newSchedulesString = "";
        for (let i = 0; i < rows.length; i++) {

            newSchedulesString += (rows[i].cells[2].innerText + "#");
        }

        cronSchedulesListControl.val(newSchedulesString);

    });

    function expressionExists(cronExpression) {
        const cronList = cronSchedulesListControl.val();
        return cronList.split("#").filter(e => e === cronExpression).length > 0;
    }

    function addSchedule(cron, humanReadableExpression, updateScheduleList = false) {

        const box = "<input type='checkbox' name='record' />";
        const markup = "<tr><td>" + box + "</td><td>" + humanReadableExpression + "</td><td>" + cron + "</td></tr>";
        $("table tbody").append(markup);

        if (updateScheduleList) {
            let cronList = cronSchedulesListControl.val();
            cronList += cron + '#';
            cronSchedulesListControl.val(cronList);
        }
    }
});




